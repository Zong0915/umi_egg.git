import React from 'react';
import { Button } from 'antd';
import axios from '../utils/axiosHelper';

const UserPage = () => {
  return <Button onClick={() => axios('/user', '/getData', { msg: 'hello' }).then(val => { console.log(val); })}>数据获取</Button>;
};

export default UserPage;
