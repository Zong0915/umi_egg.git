import axios from 'axios';

export default async function(
  prefixUrl: string,
  methodName: string,
  data: any,
) {
  let response;
  try {
    response = await axios({
      url:
        location.protocol +
        '//' +
        window.location.host +
        '/zong' +
        prefixUrl +
        methodName,
      method: 'post',
      data,
      withCredentials: true,
    });
  } catch (error) {
    console.log(error);
  }
  return response;
}
