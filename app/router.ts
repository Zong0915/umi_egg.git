// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router } = app;

  router.prefix('/zong*');

  router.post('/user/getData', controller.home.getData);
  router.get('**/**', controller.home.index);
  router.post('**/**', controller.home.index);
};
