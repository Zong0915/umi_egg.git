import { Controller } from 'egg';

class HomeController extends Controller {
  async index() {
    const { config } = this;
    const { envName, fileVersion } = config;
    const data = {
      envName,
      fileVersion,
      contextPath: 'zong',
    };
    await this.ctx.render('index.ejs', data);
  }
  getData() {
    this.ctx.body = this.service.userService.getUserName();
  }
}
export default HomeController;
