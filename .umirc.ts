import { defineConfig } from 'umi';

export default defineConfig({
  base: '/zong/',
  nodeModulesTransform: {
    type: 'none',
  },
  manifest: {
    fileName: './config/manifest.json',
    publicPath: '/public/',
  },
  outputPath: './app/public',
  publicPath: '/public/',
  fastRefresh: {},
});
