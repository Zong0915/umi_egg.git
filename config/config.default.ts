/* eslint-disable @typescript-eslint/no-unused-vars */
import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

export default (appInfo: EggAppInfo): any => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = {} as PowerPartial<EggAppConfig>;
  // 业务相关配置
  const bizConfig = {
    envName: appInfo.pkg.config.env.toLowerCase(),
  };
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1650629680875_8410';

  // add your middleware config here
  config.middleware = [];

  config.security = {
    csrf: {
      enable: false,
    },
  };
  // 开启ejs模板的使用
  config.view = {
    mapping: {
      '.ejs': 'ejs',
    },
  };
  // 本地代理，启动umi dev，监听8007端口
  config.assets = {
    publicPath: '/public',
    devServer: {
      command: 'cross-env UMI_ENV=dev umi dev --port=8007',
      port: 8007,
      env: {
        APP_ROOT: appInfo.baseDir,
        BROWSER: 'none',
        ESLINT: 'none',
        SOCKET_SERVER: 'http://127.0.0.1:8007',
        PUBLIC_PATH: 'http://127.0.0.1:8007',
      },
    },
  };

  return {
    ...config,
    ...bizConfig,
  };
};
