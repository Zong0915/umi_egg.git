import { EggPlugin } from 'egg';

const plugin: EggPlugin = {
  assets: {
    enable: true,
    package: 'egg-view-assets',
  },
  ejs: {
    enable: true,
    package: 'egg-view-ejs',
  },
  httpProxy: {
    enable: true,
    package: 'egg-http-proxy',
  },
  cors: {
    enable: true,
    package: 'egg-cors',
  },
};
export default plugin;
