/* eslint-disable indent */
/* eslint-disable @typescript-eslint/indent */
/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const { Application } = require('egg');
const pkj = require('./package.json');

const app = new Application({
  baseDir: path.resolve('./'),
});

app.ready(() => app.listen(pkj.config.port));
